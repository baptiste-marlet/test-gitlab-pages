=========================
Sphinx documentation test
=========================


Honestly, this is just a test to see how it works. Nothing fancy here.

.. code-block:: sql

    SELECT name FROM person;

.. code-block:: python

    def avg(l: list) -> float:
        return sum(l) / len(l)
