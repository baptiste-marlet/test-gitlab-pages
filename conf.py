project = "Test GitLab pages in rst"
copyright = "Baptiste Marlet"
author = "Baptiste Marlet"

release = "0.0.1"

extensions = ["sphinx_rtd_theme"]

templates_piath = ["_templates"]

exclude_patterns = []

html_theme = "sphinx_rtd_theme"

html_static_path =["_static"]
